# Weatherly-App

## Overview

Weatherly-App is a web application that provides real-time weather information for different locations. It allows users to check the current weather conditions, temperature, and forecasts based on their input.

## Features

- Search/View current weather conditions for a specific cities.
- Get detailed information about temperature, humidity, wind speed, and weather discription.
- Display users current time, date, and timezone.

## Technologies Used

- HTML5
- CSS3
- JavaScript
- [Weather API](#) - OpenWatherMap API

## How to Use

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/sapkotabasanta10/weatherly-app.git
   ```
2. Create .env to store API Key:

   ```
   WEATHER_API_KEY = your_api_key
   ```
3. Install all the dependencies: `npm install node_modules, react-dom, ...`
4. Run the web app using command : ` npm start`
