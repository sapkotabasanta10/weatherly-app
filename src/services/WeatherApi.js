const API_KEY = process.env.REACT_APP_API_KEY;
import axios from "axios";

const fetchWeather = async (address) => {
  const url = `https://api.openweathermap.org/data/2.5/weather?q=${encodeURIComponent(address)}&units=imperial&appid=${API_KEY}`;

  try {
    const response = await axios.get(url);
    // console.info("what is response?", response);

    if (response.status === 200) {
      // console.log("Got here")
      const weatherData = {
        temperature: response.data.main.temp,
        humidity: response.data.main.humidity,
        description: response.data.weather[0].description,
        wind:
          "[speed: " +
          response.data.wind.speed +
          " ,degree:" +
          response.data.wind.deg +
          "]",
      };
      
      // console.log("what is weather data", weatherData);
      return weatherData;
    } else {
      throw new Error(data.message);
    }
  } catch (error) {
    throw new Error("Failed to fetch weather data");
  }
};

export default fetchWeather;

// body: {"coord":{"lon":-77.3497,"lat":38.8501},
//       "weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],
//       "base":"stations",
//       "main":{"temp":1.7,"feels_like":-0.55,"temp_min":-1.92,"temp_max":4.17,"pressure":1030,"humidity":71},
//       "visibility":10000,"wind":{"speed":2.06,"deg":190},
//       "clouds":{"all":0},
//       "dt":1702446410,
//       "sys":{"type":2,"id":2040824,"country":"US","sunrise":1702469952,"sunset":1702504078},
//       "timezone":-18000,
//       "id":4758041,
//       "name":"Fairfax",
//       "cod":200}
