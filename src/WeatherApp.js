import React, { useState } from "react";
import SearchForm from "./components/SearchForm";
import WeatherDisplay from "./components/WeatherDisplay";
import DateTime from "./components/dateTime"
import fetchWeather from "./services/WeatherApi";

import "./styles/styles.css";

const WeatherApp = () => {
    const [weather, setWeather] = useState(null);
    const [error, setError] = useState(null);

    // console.log(RandomDog)

    const handleSubmit = async (address) => {
      try {
        const weatherData = await fetchWeather(address);
        setWeather(weatherData);
        setError(null);
      } catch (error) {
        setWeather(null);
        setError('Error fetching weather data. Please try again.');
      }
    };

    return (
      <div className="app">
        <div className="background-image"></div>
        <div className="content">
          <SearchForm onSubmit={handleSubmit} />
          {error ? (
            <div className="result-title">
              <h2> Weather Result</h2>
              <p className="error-message">{error}</p>
            </div>
          ) : (
            <WeatherDisplay weather={weather} />
          )}
          <DateTime></DateTime>

        </div>
      </div>
    );
};

export default WeatherApp;
