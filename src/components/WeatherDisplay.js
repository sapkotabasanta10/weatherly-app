import React from "react";

const WeatherDisplay = ({ weather }) => {
  return (
    <div>
      <h2>Weather Result</h2>
      {weather ? (
        <div className = "weather-display">
          <p>Temperature: {weather.temperature}°F </p>
          <p>Humidity: {weather.humidity}%</p>
          <p>Description: {weather.description}</p>
          <p>Wind: {weather.wind}</p>
        </div>
      ) : (
        <p>Input the address in input box above.</p>
      )}
    </div>
  );
};

export default WeatherDisplay;
