import React, { useState } from "react";

const SearchForm = ({onSubmit }) => {
  const [address, setAddress] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(address);
  };

  return (
    <form onSubmit={handleSubmit} className = "search-form">
      <input
        type="text"
        placeholder="Enter Your City Name"
        value={address}
        onChange={(e) => setAddress(e.target.value)}
        className = "search-input"
      />
      <button type="submit" className="search-button">Get Weather</button>
    </form>
  );
};

export default SearchForm;