import React, {useState, useEffect} from 'react'

export const DateTime = () => {
    var [date, setDate] = useState (new Date());

    useEffect( () => {
        var timer = setInterval(()=>setDate(new Date()), 1000)
        return function cleanUp(){
            clearInterval(timer);
        }
    });

    return(
        <div>
            <p> Current Time : {date.toLocaleTimeString()} </p>
            <p> Current Date : {date.toLocaleDateString()} </p>
            <p>Current TimeZone: {Intl.DateTimeFormat().resolvedOptions().timeZone}</p>

        </div>
    )

}

export default DateTime;